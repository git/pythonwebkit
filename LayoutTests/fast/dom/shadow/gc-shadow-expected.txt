This tests that the JavaScript wrapper objects of shadow DOM objects
are not prematurely garbage collected.

PASS layoutTestController.shadowRoot(ps[0]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[1]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[2]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[3]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[4]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[5]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[6]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[7]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[8]).tattoo is "I <3 WebKit"
PASS layoutTestController.shadowRoot(ps[9]).tattoo is "I <3 WebKit"
PASS successfullyParsed is true

TEST COMPLETE

