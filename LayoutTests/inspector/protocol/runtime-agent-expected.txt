Protocol stability test. It is dumping request/response pairs of RuntimeAgent functions.

-----------------------------------------------------------
RuntimeAgent.evaluate("testObject","test",false)

request:
{
    method : "Runtime.evaluate"
    params : {
        expression : "testObject"
        objectGroup : "test"
        includeCommandLineAPI : false
    }
    id : <number>
}

response:
{
    result : {
        result : {
            objectId : <string>
            hasChildren : true
            type : "object"
            className : <string>
            description : "TestObject"
        }
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.evaluate("testObject","test")

request:
{
    method : "Runtime.evaluate"
    params : {
        expression : "testObject"
        objectGroup : "test"
    }
    id : <number>
}

response:
{
    result : {
        result : {
            objectId : <string>
            hasChildren : true
            type : "object"
            className : <string>
            description : "TestObject"
        }
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.evaluateOn(<string>,"this.assignedByEvaluateOn = \"evaluateOn function works fine\";")

request:
{
    method : "Runtime.evaluateOn"
    params : {
        objectId : <string>
        expression : "this.assignedByEvaluateOn = "evaluateOn function works fine";"
    }
    id : <number>
}

response:
{
    result : {
        result : {
            type : "undefined"
            description : "undefined"
        }
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.setPropertyValue(<string>,"assignedBySetPropertyValue","true")

request:
{
    method : "Runtime.setPropertyValue"
    params : {
        objectId : <string>
        propertyName : "assignedBySetPropertyValue"
        expression : "true"
    }
    id : <number>
}

response:
{
    result : {
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.setPropertyValue(<string>,"removedBySetPropertyValue","")

request:
{
    method : "Runtime.setPropertyValue"
    params : {
        objectId : <string>
        propertyName : "removedBySetPropertyValue"
        expression : ""
    }
    id : <number>
}

response:
{
    result : {
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.getProperties(<string>,false)

request:
{
    method : "Runtime.getProperties"
    params : {
        objectId : <string>
        ignoreHasOwnProperty : false
    }
    id : <number>
}

response:
{
    result : {
        result : [
            {
                name : "assignedByEvaluateOn"
                value : {
                    type : "string"
                    description : "evaluateOn function works fine"
                }
            }
            {
                name : "assignedBySetPropertyValue"
                value : {
                    type : "boolean"
                    description : "true"
                }
            }
            {
                name : "__proto__"
                value : {
                    objectId : <string>
                    hasChildren : true
                    type : "object"
                    className : <string>
                    description : "TestObject"
                }
            }
        ]
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.releaseObject(<string>)

request:
{
    method : "Runtime.releaseObject"
    params : {
        objectId : <string>
    }
    id : <number>
}

response:
{
    result : {
    }
    id : <number>
}

-----------------------------------------------------------
RuntimeAgent.releaseObjectGroup("test")

request:
{
    method : "Runtime.releaseObjectGroup"
    params : {
        objectGroup : "test"
    }
    id : <number>
}

response:
{
    result : {
    }
    id : <number>
}

===========================================================
Coverage for RuntimeAgent
{
    evaluate : "checked"
    evaluateOn : "checked"
    getProperties : "checked"
    setPropertyValue : "checked"
    releaseObject : "checked"
    releaseObjectGroup : "checked"
}

