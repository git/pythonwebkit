Test IndexedDB: should throw when opening a database with a null name

On success, you will see a series of "PASS" messages, followed by "TEST COMPLETE".


indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB;
PASS indexedDB == null is false
IDBDatabaseException = window.IDBDatabaseException || window.webkitIDBDatabaseException;
PASS IDBDatabaseException == null is false
IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
PASS IDBTransaction == null is false
Expecting exception from indexedDB.open(null, description)
PASS Exception was thrown.
PASS code is IDBDatabaseException.NON_TRANSIENT_ERR
PASS successfullyParsed is true

TEST COMPLETE

