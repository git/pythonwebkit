EDITING DELEGATE: shouldBeginEditingInDOMRange:range from 0 of DIV > BODY > HTML > #document to 3 of DIV > BODY > HTML > #document
EDITING DELEGATE: webViewDidBeginEditing:WebViewDidBeginEditingNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: shouldChangeSelectedDOMRange:range from 2 of #text > PRE > DIV > BODY > HTML > #document to 3 of #text > PRE > DIV > BODY > HTML > #document toDOMRange:range from 3 of #text > PRE > DIV > BODY > HTML > #document to 3 of #text > PRE > DIV > BODY > HTML > #document affinity:NSSelectionAffinityDownstream stillSelecting:FALSE
EDITING DELEGATE: webViewDidChangeSelection:WebViewDidChangeSelectionNotification
EDITING DELEGATE: webViewDidChange:WebViewDidChangeNotification
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 766x44
          text run at (0,0) width 766: "This tests to see if inserting a space inside normal whitespace inside of a whitespace:pre element will collapse other"
          text run at (0,22) width 149: "spaces surrounding it."
      RenderBlock {P} at (0,60) size 784x22
        RenderText {#text} at (0,0) size 463x22
          text run at (0,0) width 463: "If successful, you should see 'a' followed by 10 spaces followed by 'b'."
      RenderBlock {HR} at (0,98) size 784x2 [border: (1px inset #000000)]
      RenderBlock {DIV} at (0,108) size 784x25 [border: (2px solid #FFAAAA)]
        RenderBlock {PRE} at (2,2) size 780x21 [border: (2px solid #AAAAFF)]
          RenderText {#text} at (2,2) size 54x17
            text run at (2,2) width 54: "a          b"
caret: position 3 of child 0 {#text} of child 1 {PRE} of child 7 {DIV} of body
