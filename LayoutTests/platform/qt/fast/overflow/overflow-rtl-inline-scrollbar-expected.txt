layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 228x22
          text run at (0,0) width 228: "This tests for a regression against "
        RenderInline {I} at (0,0) size 746x44
          RenderInline {A} at (0,0) size 352x22 [color=#0000EE]
            RenderText {#text} at (228,0) size 352x22
              text run at (228,0) width 352: "http://bugzilla.opendarwin.org/show_bug.cgi?id=6618"
          RenderText {#text} at (580,0) size 746x44
            text run at (580,0) width 4: " "
            text run at (584,0) width 162: "Inline in RTL block with"
            text run at (0,22) width 349: "overflow:auto and left border makes scroll bar appear"
        RenderText {#text} at (349,22) size 4x22
          text run at (349,22) width 4: "."
      RenderBlock {HR} at (0,60) size 784x2 [border: (1px inset #000000)]
layer at (8,78) size 784x22 clip at (18,78) size 774x22
  RenderBlock {DIV} at (0,70) size 784x22 [border: (10px solid #0000FF)]
    RenderText {#text} at (446,0) size 338x22
      text run at (446,0) width 124: "This block should "
      text run at (780,0) width 4 RTL: "."
    RenderInline {EM} at (0,0) size 21x22
      RenderText {#text} at (570,0) size 21x22
        text run at (570,0) width 21: "not"
    RenderText {#text} at (591,0) size 189x22
      text run at (591,0) width 189: " have a horizontal scroll bar"
