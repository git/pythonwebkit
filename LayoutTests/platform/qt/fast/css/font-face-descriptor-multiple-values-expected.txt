layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 67x22
          text run at (0,0) width 67: "This tests "
        RenderInline {TT} at (0,0) size 64x17
          RenderText {#text} at (67,3) size 64x17
            text run at (67,3) width 64: "@font-face"
        RenderText {#text} at (131,0) size 371x22
          text run at (131,0) width 266: " rules with font descriptors that specify "
          text run at (397,0) width 105: "multiple values."
      RenderBlock {P} at (0,38) size 784x44
        RenderText {#text} at (0,0) size 779x44
          text run at (0,0) width 779: "Helvetica is assigned to weights 100 and 900 while courier is assigned to 400. Faces are assigned to other weights are"
          text run at (0,22) width 436: "according to the heuristic in the CSS3 Web Fonts working draft."
      RenderBlock {DIV} at (0,98) size 784x48
        RenderInline {SPAN} at (0,0) size 60x46
          RenderText {#text} at (0,1) size 60x46
            text run at (0,1) width 60: "100"
        RenderText {#text} at (60,6) size 22x40
          text run at (60,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x46
          RenderText {#text} at (82,1) size 60x46
            text run at (82,1) width 60: "200"
        RenderText {#text} at (142,6) size 22x40
          text run at (142,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x46
          RenderText {#text} at (164,1) size 60x46
            text run at (164,1) width 60: "300"
        RenderText {#text} at (224,6) size 22x40
          text run at (224,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 66x40
          RenderText {#text} at (246,6) size 66x40
            text run at (246,6) width 66: "400"
        RenderText {#text} at (312,6) size 22x40
          text run at (312,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 66x40
          RenderText {#text} at (334,6) size 66x40
            text run at (334,6) width 66: "500"
        RenderText {#text} at (400,6) size 22x40
          text run at (400,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x48
          RenderText {#text} at (422,0) size 60x48
            text run at (422,0) width 60: "600"
        RenderText {#text} at (482,6) size 22x40
          text run at (482,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x48
          RenderText {#text} at (504,0) size 60x48
            text run at (504,0) width 60: "700"
        RenderText {#text} at (564,6) size 22x40
          text run at (564,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x48
          RenderText {#text} at (586,0) size 60x48
            text run at (586,0) width 60: "800"
        RenderText {#text} at (646,6) size 22x40
          text run at (646,6) width 22: " "
        RenderInline {SPAN} at (0,0) size 60x48
          RenderText {#text} at (668,0) size 60x48
            text run at (668,0) width 60: "900"
        RenderText {#text} at (0,0) size 0x0
