layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 99x22
          text run at (0,0) width 99: "This test is for "
        RenderInline {A} at (0,0) size 316x22 [color=#0000EE]
          RenderText {#text} at (99,0) size 316x22
            text run at (99,0) width 316: "http://bugs.webkit.org/show_bug.cgi?id=14498"
        RenderText {#text} at (415,0) size 4x22
          text run at (415,0) width 4: "."
      RenderBlock {P} at (0,38) size 784x22
        RenderText {#text} at (0,0) size 438x22
          text run at (0,0) width 438: "Click inside the right border. The caret should move after \x{201C}bar\x{201D}."
      RenderBlock (anonymous) at (0,76) size 784x22
        RenderInline {SPAN} at (0,0) size 69x42 [border: (10px solid #0000FF)]
          RenderText {#text} at (0,0) size 0x0
          RenderBlock {DIV} at (10,0) size 21x22
            RenderText {#text} at (0,0) size 21x22
              text run at (0,0) width 21: "foo"
          RenderText {#text} at (31,0) size 4x22
            text run at (31,0) width 4: " "
          RenderBlock {DIV} at (35,0) size 24x22
            RenderText {#text} at (0,0) size 24x22
              text run at (0,0) width 24: "bar"
          RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (0,0) size 0x0
caret: position 3 of child 0 {#text} of child 3 {DIV} of child 5 {SPAN} of body
