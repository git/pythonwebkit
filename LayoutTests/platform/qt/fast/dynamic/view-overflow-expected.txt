layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 57x22
          text run at (0,0) width 57: "Test for "
        RenderInline {I} at (0,0) size 648x22
          RenderInline {A} at (0,0) size 304x22 [color=#0000EE]
            RenderText {#text} at (57,0) size 304x22
              text run at (57,0) width 304: "http://bugs.webkit.org/show_bug.cgi?id=13145"
          RenderText {#text} at (361,0) size 344x22
            text run at (361,0) width 4: " "
            text run at (365,0) width 340: "Regression: Scrollbar not resizing after display none"
        RenderText {#text} at (705,0) size 4x22
          text run at (705,0) width 4: "."
      RenderBlock {P} at (0,38) size 784x22
        RenderText {#text} at (0,0) size 334x22
          text run at (0,0) width 334: "The window should not have a vertical scroll bar."
layer at (8,100) size 100x100
  RenderBlock (positioned) {DIV} at (8,100) size 100x100
    RenderBlock {DIV} at (0,0) size 100x100 [bgcolor=#008000]
